// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
#include "fstream"
#include "vector"
#include "string"
#include "tuple"
#include "sstream"
#include "iomanip"

using namespace std;

class Osoba
{
protected:
	string identyfikator;
	string imie;
	string nazwisko;
	string numerKonta;
public:
	string getIdentyfikator()
	{
		return identyfikator;
	}
	string getImie()
	{
		return imie;
	}
	string getNazwisko()
	{
		return nazwisko;
	}
	string getNumerKonta()
	{
		return numerKonta;
	}

};

class Student : public Osoba
{
protected:
	int rokStudiow;
	vector<tuple<string, int>> oceny;
public:
	vector<tuple<string, int>> *getOceny()
	{
		return &oceny;
	}
	Student(string, string, string, string, int);
	void wypisz();
	void wypiszOcena();
	int getRokStudiow() {
		return rokStudiow;
	}
};
Student::Student(string identyfikator, string imie, string nazwisko, string numerKonta, int rokStudiow)
{
	this->identyfikator = identyfikator;
	this->imie = imie;
	this->nazwisko = nazwisko;
	this->numerKonta = numerKonta;
	this->rokStudiow = rokStudiow;
}
void Student::wypisz()
{
	cout << identyfikator << "\t" << imie << "\t" << nazwisko << "\t" << rokStudiow << "\t" << numerKonta << endl;
}
void Student::wypiszOcena()
{
	for (int i = 0; i < oceny.size(); i++)
	{
		cout << get<0>(oceny.at(i)) << ": " << get<1>(oceny.at(i)) << endl;
	}
}

class Pracownik : virtual public Osoba
{
protected:
	string stanowisko;
public:
	Pracownik()
	{

	}
	Pracownik(string, string, string, string, string);
	virtual string getStanowisko()
	{
		return stanowisko;
	}
};
Pracownik::Pracownik(string identyfikator, string imie, string nazwisko, string numerKonta, string stanowisko)
{
	this->identyfikator = identyfikator;
	this->imie = imie;
	this->nazwisko = nazwisko;
	this->numerKonta = numerKonta;
	this->stanowisko = stanowisko;
}

class Uczony :public Pracownik
{
protected:
	string stopienNaukowy;
public:
	Uczony(string, string, string, string, string, string);
	virtual string getStopienNaukowy()
	{
		return stopienNaukowy;
	}
};
Uczony::Uczony(string identyfikator, string imie, string nazwisko, string numerKonta, string stanowisko, string stopienNaukowy)
{
	this->identyfikator = identyfikator;
	this->imie = imie;
	this->nazwisko = nazwisko;
	this->numerKonta = numerKonta;
	this->stanowisko = stanowisko;
	this->stopienNaukowy = stopienNaukowy;
}

class Uczelnia
{
protected:
	vector<Student> studenci;
	vector<tuple<string, double>> wyplaty;
	vector<Pracownik *> pracownicy;
	vector<tuple<string, string, double>> liczPlatnosci();
	vector<tuple<string, string, double>> sortujPlatnosci(vector<tuple<string, string, double>>);
public:
	Uczelnia()
	{
		czytajStudenci(&studenci, "students.csv");
		czytajOceny(&studenci, "grades.csv");
		czytajWyplaty(&wyplaty, "salaries.csv");
		czytajPracownicy(&pracownicy, "employees.csv");
	}
	~Uczelnia()
	{
		for (int i = 0; i < pracownicy.size(); i++)
		{
			delete pracownicy.at(i);
		}
	}
	void savePayments(string nazwa);
	void czytajStudenci(vector<Student> *, string nazwa);
	void czytajOceny(vector<Student> *, string nazwa);
	void czytajWyplaty(vector<tuple<string, double>> *, string nazwa);
	void czytajPracownicy(vector<Pracownik *> *, string nazwa);
	void wypiszWszystko();
};
string czytajDo(char dzielnik, int n, string text)
{
	string slowo = "";
	int i = 0;
	int z = 0;
	char znak = text.at(i);
	while (z <= n)
	{
		if (znak == dzielnik)
		{
			z++;
		}
		else if (z == n)
		{
			slowo += znak;
		}
		i++;
		try
		{
			znak = text.at(i);
		}
		catch (out_of_range)
		{
			break;
		}
	}
	return slowo;
}
void Uczelnia::czytajStudenci(vector<Student> *studenci, string nazwa)
{
	ifstream plik;
	plik.open(nazwa);
	string linia;
	string slowo;
	int i = 0;
	while (getline(plik, linia))
	{
		string indentyfikator = czytajDo('|', 0, linia);
		string imie = czytajDo('|', 1, linia);
		string nazwisko = czytajDo('|', 2, linia);
		string numerKonta = czytajDo('|', 3, linia);
		int rokStudiow = 0;
		try
		{
			rokStudiow = stoi(czytajDo('|', 4, linia));
		}
		catch (out_of_range)
		{
		}
		Student student(indentyfikator, imie, nazwisko, numerKonta, rokStudiow);
		studenci->push_back(student);
		i++;
	}
	plik.close();
}
void Uczelnia::czytajOceny(vector<Student> *studenci, string nazwa)
{
	ifstream plik;
	plik.open(nazwa);
	string linia;
	string slowo;
	int i = 0;
	while (getline(plik, linia))
	{
		string identyfikator = czytajDo('|', 0, linia);
		string przedmiot = czytajDo('|', 1, linia);
		double ocena = 0;
		try
		{
			ocena = stod(czytajDo('|', 2, linia));
		}
		catch (out_of_range)
		{
		}
		for (int n = 0; n < studenci->size(); n++)
		{
			if (studenci->at(n).getIdentyfikator() == identyfikator)
			{
				studenci->at(n).getOceny()->push_back(make_tuple(przedmiot, ocena));
				break;
			}
		}

		i++;
	}
	plik.close();
}
void Uczelnia::czytajWyplaty(vector<tuple<string, double>> *wyplaty, string nazwa)
{
	ifstream plik;
	plik.open(nazwa);
	string linia;
	string slowo;
	int i = 0;
	while (getline(plik, linia))
	{
		string stanowisko = czytajDo('|', 0, linia);
		double wartosc = 0;
		try
		{
			wartosc = stod(czytajDo('|', 1, linia));
		}
		catch (out_of_range)
		{
		}

		wyplaty->push_back(make_tuple(stanowisko, wartosc));

		i++;
	}
	plik.close();
}
void Uczelnia::czytajPracownicy(vector<Pracownik *> *pracownicy, string nazwa)
{
	ifstream plik;
	plik.open(nazwa);
	string linia;
	string slowo;
	int i = 0;
	while (getline(plik, linia))
	{
		string identyfikator = czytajDo('|', 0, linia);
		string imie = czytajDo('|', 1, linia);
		string nazwisko = czytajDo('|', 2, linia);
		string numerKonta = czytajDo('|', 3, linia);
		string stanowisko = czytajDo('|', 4, linia);
		string stopienNaukowy = czytajDo('|', 5, linia);

		if (stopienNaukowy == "-")
		{
			Pracownik *pracownik = new Pracownik(identyfikator, imie, nazwisko, numerKonta, stanowisko);
			pracownicy->push_back(pracownik);
		}
		else {
			Uczony *uczony = new Uczony(identyfikator, imie, nazwisko, numerKonta, stanowisko, stopienNaukowy);
			pracownicy->push_back(uczony);
		}


		i++;
	}
	plik.close();
}
void Uczelnia::wypiszWszystko() {
	cout << "Studenci:" << endl;
	for (int i = 0; i < studenci.size(); i++)
	{
		cout << studenci.at(i).getIdentyfikator() << " " << studenci.at(i).getImie() << " " << studenci.at(i).getNazwisko() << " rok studiow " << studenci.at(i).getRokStudiow() << "\t" << studenci.at(i).getNumerKonta() << endl;
		studenci.at(i).wypiszOcena();
	}

	cout << endl << "Pracownicy:" << endl;
	for (int i = 0; i < pracownicy.size(); i++)
	{
		cout << pracownicy.at(i)->getIdentyfikator() << " " << pracownicy.at(i)->getStanowisko() << " ";
		Uczony *foo = dynamic_cast<Uczony *>(pracownicy.at(i));
		if (foo != NULL)
		{
			cout << foo->getStopienNaukowy() << " ";
		}
		cout << pracownicy.at(i)->getImie() << " " << pracownicy.at(i)->getNazwisko() << "\t" << pracownicy.at(i)->getNumerKonta() << endl;


		cout << endl;
	}

	cout << endl << "Wyplaty: " << endl;
	for (int i = 0; i < wyplaty.size(); i++)
	{
		cout << get<0>(wyplaty.at(i)) << ": " << get<1>(wyplaty.at(i)) << endl;
	}

	cout << endl << endl;
}
vector<tuple<string, string, double>> Uczelnia::sortujPlatnosci(vector<tuple<string, string, double>> platnosci) {
	if (platnosci.empty())
	{
		return platnosci;
	}
	vector<tuple<string, string, double>> posortowaneWyplaty;

	while (!platnosci.empty())
	{
		double wartoscMin = get<2>(platnosci.at(0));
		int indeksMin = 0;
		for (int i = 0; i < platnosci.size(); i++)
		{
			if (wartoscMin > get<2>(platnosci.at(i)))
			{
				indeksMin = i;
				wartoscMin = get<2>(platnosci.at(i));
			}
		}
		posortowaneWyplaty.push_back(platnosci.at(indeksMin));
		platnosci.erase(platnosci.begin() + indeksMin);
	}
	return posortowaneWyplaty;
}
vector<tuple<string, string, double>> Uczelnia::liczPlatnosci()
{
	vector<tuple<string, string, double>> platnosci;

	for (int i = 0; i < pracownicy.size(); i++)
	{
		string numerKonta = pracownicy.at(i)->getNumerKonta();

		string tytul = "Wynagrodzenie: ";
		Uczony *uczony = dynamic_cast<Uczony *>(pracownicy.at(i));
		if (uczony != NULL)
		{
			tytul += uczony->getStopienNaukowy() + " ";
		}
		tytul += pracownicy.at(i)->getImie() + " " + pracownicy.at(i)->getNazwisko();

		double wartosc = 0;
		for (int n = 0; n < wyplaty.size(); n++)
		{
			if (get<0>(wyplaty.at(n)) == pracownicy.at(i)->getStanowisko())
			{
				wartosc = get<1>(wyplaty.at(n));
			}
		}

		platnosci.push_back(make_tuple(numerKonta, tytul, wartosc));
	}

	for (int i = 0; i < studenci.size(); i++) {
		double ocena = 0;
		int n = 0;
		for (n = 0; n < studenci.at(i).getOceny()->size(); n++) {
			ocena += get<1>(studenci.at(i).getOceny()->at(n));
		}
		ocena /= n;

		double treshold = 4;
		if (ocena >= treshold) {
			string numerKonta = studenci.at(i).getNumerKonta();
			string tytul = "Stypendium za wyniki w nauce; " + studenci.at(i).getImie() + " " + studenci.at(i).getNazwisko();
			double wartosc = 400;
			platnosci.push_back(make_tuple(numerKonta, tytul, wartosc));
		}
	}

	return sortujPlatnosci(platnosci);
}

void Uczelnia::savePayments(string nazwa)
{
	vector<tuple<string, string, double>> platnosci = liczPlatnosci();

	ofstream plik;
	plik.open(nazwa);

	for (int i = 0; i < platnosci.size(); i++)
	{
		stringstream str;
		str << get<0>(platnosci.at(i));
		str << "|";
		str << get<1>(platnosci.at(i));
		str << "|";
		str << fixed << setprecision(2) << get<2>(platnosci.at(i));
		str << "\n";
		plik << str.str();
	}

	plik.close();
}

int main()
{
	Uczelnia politechnika;
	politechnika.wypiszWszystko();
	politechnika.savePayments("transactions.csv");
	return 0;
}
