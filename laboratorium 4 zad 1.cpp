// laboratorium 4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>

using namespace std;

class Polygon
{
public:
	virtual double perimiter() = 0;
	virtual double area() = 0;
	virtual bool hasGreaterArea(Polygon &) = 0;
};

class Point
{
public:
	double x, y;
	Point(double = 0, double = 0);
};
Point::Point(double xx, double yy)
{
	x = xx;
	y = yy;
}

double odleglosc(Point pkt1, Point pkt2)
{
	return pow(pkt1.x - pkt2.x, 2) + pow(pkt1.y - pkt2.y, 2);
}

class Triangle : public Polygon
{
private:
	Point p1;
	Point p2;
	Point p3;
public:
	Triangle(Point, Point, Point);
	virtual double perimiter();
	virtual double area();
	virtual bool hasGreaterArea(Polygon &);
};
Triangle::Triangle(Point p1, Point p2, Point p3)
	:p1(p1), p2(p2), p3(p3)
{
}
double Triangle::perimiter()
{
	double a = sqrt(odleglosc(p1, p2));
	double b = sqrt(odleglosc(p1, p3));
	double c = sqrt(odleglosc(p3, p2));
	return a + b + c;
}
double Triangle::area()
{
	double a = sqrt(odleglosc(p1, p2));
	double b = sqrt(odleglosc(p1, p3));
	double c = sqrt(odleglosc(p3, p2));
	double p = perimiter() / 2;
	return (sqrt(p*(p - a)*(p - b)*(p - c)));
}
bool Triangle::hasGreaterArea(Polygon &figura)
{
	return (area() > figura.area());
}

class Rectangle : public Polygon
{
private:
	Point p1;
	Point p2;
	Point p3;
public:
	Rectangle(Point = 0, Point = 0, Point = 0);
	virtual double perimiter();
	virtual double area();
	virtual bool hasGreaterArea(Polygon &);
};
Rectangle::Rectangle(Point p1, Point p2, Point p3)
	:p1(p1), p2(p2), p3(p3)
{
}
double Rectangle::perimiter()
{
	double a = sqrt(odleglosc(p1, p2));
	double b = sqrt(odleglosc(p2, p3));
	return 2 * (a + b);
}
double Rectangle::area()
{
	double a = sqrt(odleglosc(p1, p2));
	double b = sqrt(odleglosc(p2, p3));
	return a * b;
}
bool Rectangle::hasGreaterArea(Polygon &figura)
{
	return (area() > figura.area());
}

class Rhombus : public Polygon
{
private:
	Point p1;
	Point p2;
	Point p3;
	Point p4;
public:
	Rhombus(Point = 0, Point = 0, Point = 0, Point = 0);
	virtual double perimiter();
	virtual double area();
	virtual bool hasGreaterArea(Polygon &);
};
Rhombus::Rhombus(Point p1, Point p2, Point p3, Point p4)
	:p1(p1), p2(p2), p3(p3), p4(p4)
{
}
double Rhombus::perimiter()
{
	double a = sqrt(odleglosc(p1, p2));
	return 4 * a;
}
double Rhombus::area()
{
	double e = sqrt(odleglosc(p1, p4));
	double f = sqrt(odleglosc(p2, p3));

	return (e*f)/2;
}
bool Rhombus::hasGreaterArea(Polygon &figura)
{
	return (area() > figura.area());
}

class Square : virtual public Rectangle, virtual public Rhombus
{
private:
	Point p1;
	Point p2;
	Point p3;
public:
	Square(Point, Point, Point);
	virtual double perimiter();
	virtual double area();
	virtual bool hasGreaterArea(Polygon &figura);
};
Square::Square(Point p1, Point p2, Point p3)
	:p1(p1), p2(p2), p3(p3)
{
}
double Square::perimiter()
{
	double a = sqrt(odleglosc(p1, p2));
	return 4 * a;
}
double Square::area()
{
	double a = sqrt(odleglosc(p1, p2));

	return a * a;
}
bool Square::hasGreaterArea(Polygon &figura)
{
	return (area() > figura.area());
}

double suma_obwodow_figur(vector <Polygon*> polygons)
{
	double suma = 0;
	for (int i = 0; i < polygons.size(); i++)
	{
		suma += polygons[i]->perimiter();
	}
	return suma;
}

int main()
{
	Triangle t1(Point(0, 0), Point(0, 10), Point(30, 0));

	Polygon *wsk1;
	wsk1 = &t1;
	cout <<"trojkat obwod:"<< wsk1->perimiter() << ", pole:" << wsk1->area() << endl;

	Rectangle pr1(Point(0, 0), Point(0, 10), Point(10, 10));
	Polygon *wsk2;
	wsk2 = &pr1;
	cout <<"prostokat obwod:"<< wsk2->perimiter() << ", pole:" << wsk2->area() << endl;

	bool czyWiekszy = wsk2->hasGreaterArea(*wsk1);
	if (czyWiekszy)
		cout << "Prostokat jest wiekszy od trojkata" << endl;
	else
		cout << "Prostokat nie jest wiekszy od trojkata" << endl;

	vector <Polygon*> polygons;
	polygons.push_back(&pr1);
	polygons.push_back(&Rectangle(Point(0, 0), Point(0, 10), Point(10, 10)));
	polygons.push_back(&Rectangle(Point(0, 0), Point(0, 10), Point(10, 10)));

	cout << "suma obwodow prostokatow przekazanych do funcji wynosi: " << suma_obwodow_figur(polygons) << endl;

	Rhombus rm1(Point(0, 0), Point(0, 10), Point(10, 0), Point(10, 10));
	Polygon *wsk3;
	wsk3 = &rm1;
	cout <<"romb obwod:"<< wsk3->perimiter() << ", pole:" << wsk3->area() << endl;

	Square kw1(Point(0, 0), Point(0, 20), Point(20, 0));
	Square *wsk4;
	wsk4 = &kw1;
	cout <<"kwadrat obwod:"<< wsk4->perimiter() << ", pole:" << wsk4->area() << endl;

	cout << "zajmowana pamiec trojkat: "<< sizeof(t1) << endl;
	cout << "zajmowana pamiec prostokat: " << sizeof(pr1) << endl;
	cout << "zajmowana pamiec romb: " << sizeof(rm1) << endl;
	cout << "zajmowana pamiec kwadrat: " << sizeof(kw1) << endl;
    return 0;
}

